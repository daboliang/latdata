---
title: 'Featuretools: ensighten - device'
output:
  html_document:
    df_print: paged
---

## Set up Python env, load data frame using data.table

```{r setup}
library(reticulate)
use_condaenv('py36')
device <- data.table::fread('device.csv.gz')
```

## create device entity with *device_id* as unique identifier

```{python, results='hide'}
# https://stackoverflow.com/a/52468712/8122752
import featuretools as ft

es = ft.EntitySet(id="device_event")
es.entity_from_dataframe(entity_id="events",
                         dataframe=r.device,
                         index="device_id",
                         make_index=True)

entity_df = es["events"].df
```


## Convert Entity (Pandas' DataFrame) to R, then pin to aws S3
 
```{r, results='hide'}
res<-py$entity_df

pins::board_register(
  board = 's3',
  bucket = "caltimes-data-science",
  key = Sys.getenv('aws_access_key_id'),
  secret = Sys.getenv('aws_secret_access_key')
)

pins::pin(res, 'pins/featuretools/device1/feature-matrix', board = 's3')

```

You can optionally open up https://shiny.data.caltimes.io/content/107/ and see the new pinned item in the dropdown list

