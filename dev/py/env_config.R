library(reticulate)

conda_list()

conda_create('py36', packages = c('python=3.6', 'pip'))
conda_install('py36', 'featuretools')

use_condaenv("py36")
conda_install('py36', 'python-graphviz')

conda_install('py36', c('pandas', 'matplotlib', 'scipy', 'numpy', 'scikit-learn', 'seaborn', 'tqdm'))

