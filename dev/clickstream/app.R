if(!require('ggfortify')){
  install.packages("ggfortify")
}
library(shiny)
library(correlationfunnel)
library(plotly)


# Shiny module functions --------------------------------------------------------

CorrFunnelPlotInputCSS <- function(){
  shiny::tags$style(shiny::HTML('
    * {
    font-family: Georgia;
    }
  '))
}

CorrFunnelPlotInput <- function(id) {
  # Create a namespace function using the provided id
  ns <- NS(id)

  tagList(
    fluidPage(
      fluidRow(
        column(12,
               plotlyOutput(ns('corr_funnel_plot'))
        )
      ),
      fluidRow(
        column(6,
               selectInput(ns('target'), label = "Select Target", choices = c('Churn__Yes', 'Churn__No'), multiple = F)
        ),
        column(6,
               uiOutput(ns('featureDropdown'))
        )
      ),
      fluidRow(
        column(12,
               DT::dataTableOutput(ns('dt'), width = '100%')
        )
      )
    ),
    CorrFunnelPlotInputCSS()

  )
}

CorrFunnelPlot <- function(input, output, session) {

  ns <- session$ns
  output$featureDropdown <- renderUI({
    selectInput(ns('feature'), label = 'Feature(s)',
                choices = colnames(correlationfunnel::customer_churn_tbl %>% select(-c(`customerID`, `Churn`, `TotalCharges`))),
                multiple = TRUE, selected = c('gender', 'StreamingTV', 'PhoneService', 'Contract'))
  })
  # The user's data, parsed into a data frame
  dataInput <- reactive({
    correlationfunnel::customer_churn_tbl %>%
      select(c('Churn', input$feature))
  })

  dataInput_onehot <- reactive({
    dataInput() %>% binarize()
  })

  output$dt <- DT::renderDataTable({
    dataInput()
  }, options = list(scrollX = TRUE))

  output$corr_funnel_plot <- renderPlotly({
    correlationfunnel::plot_correlation_funnel(interactive = TRUE, alpha = 0.7,
                                               data = dataInput_onehot() %>% correlate(target = input$target))
  })
}



# shinyapp -----------------------------------------------------------

ui <- fluidPage(
  wellPanel(
    CorrFunnelPlotInput("corrFunnelPlot")
  )
)

server <- function(input, output, session) {
  callModule(CorrFunnelPlot, "corrFunnelPlot")
}

shinyApp(ui, server)

