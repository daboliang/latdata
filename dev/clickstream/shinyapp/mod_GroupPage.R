# Shiny module functions --------------------------------------------------------

GroupPageCSS <- function(){
  shiny::tags$style(shiny::HTML('
    * {
    font-family: Georgia;
    }
  '))
}

GroupPageInput <- function(id) {
  # Create a namespace function using the provided id
  ns <- NS(id)
  material_page(
    include_nav_bar = FALSE,
    material_row(
      material_column(
        width = 12,
        material_card(
          DT::dataTableOutput(ns('group_summary'))
        )
      )
    ),
    GroupPageCSS()
  )

}

GroupPage <- function(input, output, session, segmented_data) {

  ns <- session$ns
  ensighten_pinned <- pins::pin_get(name = 'sample_ensighten_2020', board = 'rsconnect')


  raw_ensighten <- reactive({
    ensighten_pinned %>% semi_join(segmented_data(), by = "ssor_id")
  })

  output$group_summary <- DT::renderDataTable(

    # dataSummary()
    DT::datatable(raw_ensighten() %>% select(ssor_id, geo_postal_code, section, headline, slug), rownames = FALSE, style = 'material',
                  options = list(dom = 'tp',
                                 scrollX = TRUE,
                                 autoWidth = TRUE,
                                 columnDefs = list(list(width = '200px', targets = "_all")),
                                 pageLength = 10,
                                 lengthMenu = c(5, 10, 50))
    )
  )



}

