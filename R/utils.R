#' @importFrom magrittr %>%
#' @export
magrittr::`%>%`

#' start R from clean slate
#'
#' @return
#' @export
#'
#' @examples
.re <- function(){

  # detach and unloadNamespace all pkgs if loaded
  invisible(lapply(names(sessionInfo()$otherPkgs), function(pkgs)
    detach(
      paste0('package:', pkgs),
      character.only = T,
      unload = T,
      force = T
    )))

  # remove all objects and restart R session
  rm(list = ls(envir = .GlobalEnv, all.names = TRUE), envir = .GlobalEnv)
  invisible(.rs.restartR())
}
